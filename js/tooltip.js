$(document).ready(function () {
	var windowHeight = $(window).height();
	$(document).on('scroll', function () {
		$('.s-prices').each(function () {
			var self = $(this),
				height = self.offset().top + self.height() / 2 - windowHeight / 2;
			if ($(document).scrollTop() >= height) {
				$(".tooltip").delay(1800).fadeIn(1200)
							 .parents('.prices-row').addClass('row-expanded'); // Медленно выводим изображение
			};
		});
	});
});


/*$(document).on('scroll', function () {
		$(".tooltip").fadeIn(800); // Медленно выводим изображение
	});
});*/

$(document).ready(function () { // Ждём загрузки страницы
	$(".tooltip_close, .tooltip").click(function () { // Событие клика на затемненный фон	   
		var tooltip = $(this).hasClass('tooltip') ? $(this) : $(this).parents('.tooltip');
		tooltip.remove()
			   .parents('.prices-row').removeClass('row-expanded'); // Медленно убираем всплывающее окно
	});
});